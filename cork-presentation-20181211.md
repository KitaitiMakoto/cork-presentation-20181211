# パララックス、またはスクロールで遊ぶ

date
:   2018/12/11

allotted-time
:   5m

# パララックスとは

* [写真を読む夜](https://yomu.lp.yamauchihiroyasu.jp/)
* [Look Book](http://melanie-f.com/en/)

など

# 視差効果

とは

# ウェブ的には

スクロールで遊んでるだけ
例： [skrollr](https://prinzhorn.github.io/skrollr/)

# 視差効果じゃなくていい

例：[レッドドラゴン](https://sai-zen-sen.jp/works/special/reddragon/01/01.html)

# おまけ

[parallax.js](http://matthew.wagerfield.com/parallax/)

# おまけ（２）

[謎と旅する女](https://sai-zen-sen.jp/works/fictions/gamekids2013/01/01.html)
